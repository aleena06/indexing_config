package com.example.demo;

import java.util.Date;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@RestController
@RequestMapping("/log")
public class HelloController 
{
	private static final Logger LOG = LogManager.getLogger(HelloController.class.getName());
	
	@GetMapping("/getName")
	public String getResponse1(@RequestParam("name") String name) {
		LOG.debug("RequestParam - " + name);
		return name + " - " + new Date();
	}
	
	@GetMapping("/fetch")
	public String getResponse2() throws InterruptedException {
		return "Hello";
	}
	
	@GetMapping("/exception")
	public void throwException(){
		try {
			throw new Exception();
		}
		catch(Exception e) {
			LOG.error("Exception - " , e);
		}
	}
}